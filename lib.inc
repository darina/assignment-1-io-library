section .text

%define EXIT 60
%define NEWLINE 0xA
%define TAB 0x9
%define NO 0x20

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
   xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0  ; если не 0, инкрементируем rax
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rsi, rdi
    mov rdi, 1
    mov rdx, rax
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r10, 10
    mov rsi, rsp
    dec rsp
    mov [rsp], byte 0
    .loop:
	    xor rdx, rdx
	    div r10
	    add rdx, '0'
        dec rsp
	    mov [rsp], dl
        cmp rax, 0
	    jnz .loop
    .print:
        mov rdi, rsp
        push rsi
        call print_string
        pop rsp
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
	    mov al, [rsi]
	    cmp al, [rdi]
	    jne .diff
        inc rsi
	    inc rdi
	    cmp al, 0
	    jne .loop
	    mov rax, 1
	    ret
    .diff:
	    xor rax, rax
	    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    xor rdi, rdi
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rdi
    push rsi
    .check_spaces:
        call read_char
        cmp rax, NO
        je .check_spaces
        cmp rax, TAB
        je .check_spaces
        cmp rax, NEWLINE
        je .check_spaces
        pop r11 
        pop r10 
        push r12
        xor r12, r12   
    .read_word:
        cmp r11, r12
        je .buffer_overflow
        mov [r10+r12], rax

	test rax, rax
        je .end
        cmp rax, ' '
        je .end
        cmp rax, TAB
        je .end
        cmp rax, NEWLINE
        je .end
        inc r12
        push r10
        push r11
        call read_char
        pop r11
        pop r10
        jmp .read_word      
    .end:
        mov byte[r10+r12], 0
        mov rax, r10
        mov rdx, r12
        pop r12
        ret 
    .buffer_overflow:
        pop r12
        xor rax, rax
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r10, r10
    mov r10b, byte[rdi]
    cmp r10b, '0'
    jb .error
    cmp r10b, '9'
    ja .error
    sub r10b, '0'
    mov al, r10b
    mov rdx, 1
    .loop:
	    mov r10b, byte[rdi+rdx]
	    cmp r10b, '0'
	    jb .end
	    cmp r10b, '9'
	    ja .end
	    inc rdx
	    imul rax, NEWLINE
	    sub r10b, '0'
	    add rax, r10
	    jmp .loop
    .error:
	    xor rax, rax
	    xor rdx, rdx
	    ret
    .end:
	    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'
    je .minus
    jmp parse_uint
    .minus:
	    inc rdi
	    call parse_uint
	    inc rdx
	    neg rax
	    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r10, r10
    call string_length
    add rax, 1
    cmp rdx, rax ; size checking
    jl .fail
    .loop:
	    mov r10b, byte[rdi]
	    mov byte[rsi], r10b
	    test r10, r10
	    je .end
	    inc rax
	    inc rsi
	    inc rdi
	    jmp .loop
    .fail:
	    xor rax, rax
    .end:
	    ret
